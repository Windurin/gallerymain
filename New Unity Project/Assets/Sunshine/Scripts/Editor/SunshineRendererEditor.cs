using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

[CustomEditor (typeof(SunshineRenderer))]
public class SunshineRendererEditor : Editor
{
		public override void OnInspectorGUI ()
		{
				EditorGUILayout.HelpBox ("This component allows Sunshine to respect a Renderer's \"Receieve Shadows\" option.", MessageType.None);
		}
}
