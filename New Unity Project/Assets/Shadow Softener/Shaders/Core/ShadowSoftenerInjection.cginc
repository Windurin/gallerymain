#if defined(SPOT)
#if defined(SHADOWS_DEPTH)
	#define SOFTENER_PREPASS
	#define SOFTENER_SPOT
	#include "ShadowSoftenerConfig.cginc"
	#include "../ShadowSoftener.cginc"
	#define UnitySampleShadowmap(coord) SoftenerSampleShadowmap(coord)
	inline float SoftenerSampleShadowmap(float4 coord)
	{
		return SOFTENER_CUSTOM_FILTER(coord.xyz / coord.w);
	}
#endif //SHADOWS_DEPTH
#endif //SPOT

#if defined (POINT) || defined (POINT_COOKIE)
#if defined(SHADOWS_CUBE)
	#define SOFTENER_PREPASS
	#define SOFTENER_POINT
	#include "ShadowSoftenerConfig.cginc"
	#include "../ShadowSoftener.cginc"
	#define UnitySampleShadowmap(vec) SoftenerSampleShadowmap(vec)
	inline float SoftenerSampleShadowmap(float3 vec)
	{
		float mydist = length(vec) * _LightPositionRange.w;
		//mydist *= 0.97; // bias
		return SOFTENER_CUSTOM_FILTER(float4(vec, mydist));
	}
#endif //SHADOWS_CUBE
#endif